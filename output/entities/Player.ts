import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Score } from "./Score";

@Index("player_name", ["playerName"], { unique: true })
@Entity("player", { schema: "application" })
export class Player {
  @Column("varchar", { name: "player_name", unique: true, length: 50 })
  playerName: string;

  @PrimaryGeneratedColumn({ type: "int", name: "player_id" })
  playerId: number;

  @OneToMany(() => Score, (score) => score.player)
  scores: Score[];
}
