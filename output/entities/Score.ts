import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Player } from "./Player";

@Index("fk_score_player_id", ["playerId"], {})
@Entity("score", { schema: "application" })
export class Score {
  @PrimaryGeneratedColumn({ type: "int", name: "score_id" })
  scoreId: number;

  @Column("int", { name: "score", default: () => "'0'" })
  score: number;

  @Column("int", { name: "player_id", default: () => "'0'" })
  playerId: number;

  @Column("timestamp", { name: "date", default: () => "CURRENT_TIMESTAMP" })
  date: Date;

  @ManyToOne(() => Player, (player) => player.scores, {
    onDelete: "RESTRICT",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "player_id", referencedColumnName: "playerId" }])
  player: Player;
}
