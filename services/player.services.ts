import { Body, Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Player } from "output/entities/Player";
import { AddPlayerDto } from "src/dtos/addPlayer.dto";
import { Repository } from "typeorm";

@Injectable()
export class PlayerService {
    constructor(
        @InjectRepository(Player)
        private readonly player: Repository<Player>,
    ) {}

    async getData(username: string):Promise<Player>{
        let player:Player  = await this.player.findOne({
            where:{
              playerName: username,
        },       
           relations:["scores"]   
        })  
            if(player === undefined){
                   throw new NotFoundException('Player not exist');
               }
            return player;   
  }

     //ADD NEW PLAYER
 async add(@Body()data:AddPlayerDto):Promise<Player>{
    let newPlayer:Player = new Player(); 
    newPlayer.playerName = data.username; 
    var savedPlayer = await this.player.save(newPlayer)
    return savedPlayer;
    
}
}
