import { Body, Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Score } from "output/entities/Score";
import { AddPlayerDto } from "src/dtos/addPlayer.dto";
import { AddScoreDto } from "src/dtos/addScore.dto";
import { Repository } from "typeorm";

@Injectable()
export class ScoreService {
    constructor(
        @InjectRepository(Score)
        private readonly score: Repository<Score>,
    ) {}

     //ADD NEW score
 async addScore(@Body()data:AddScoreDto):Promise<Score>{
    let newScore:Score = new Score(); 
    newScore.score = data.score;
    newScore.playerId = data.playerId; 
    var savedScore = await this.score.save(newScore)
    return savedScore;   
}
}