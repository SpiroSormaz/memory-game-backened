import { Body, Controller, Get, HttpException, HttpStatus, Param, Post } from '@nestjs/common';
import { PlayerService } from 'services/player.services';
import { ScoreService } from 'services/score.service';
import { AppService } from './app.service';
import { AddPlayerDto } from './dtos/addPlayer.dto';
import { AddScoreDto } from './dtos/addScore.dto';


@Controller()
export class AppController {
  constructor(
    private readonly playerService: PlayerService,
    private readonly scoreService: ScoreService
    
){}

  @Get('api/player/:username')
  getPlayerl(@Param('username')username:string) {
    return this.playerService.getData(username);
  }

     // ADD NEW PLAYER
@Post("api/addPlayer")
async add(@Body()data:AddPlayerDto){
    return await this.playerService.add(data);
}

     // ADD NEW SCORE
 @Post("api/addScore")
  async addScore(@Body()data:AddScoreDto){
     return await this.scoreService.addScore(data);
}

}
