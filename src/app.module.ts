import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule} from '@nestjs/typeorm'
import { DatabaseConfiguration } from 'config/database';
import { PlayerService } from 'services/player.services';

import { ScoreService } from 'services/score.service';
import { Player } from 'output/entities/Player';
import { Score } from 'output/entities/Score';


@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: DatabaseConfiguration.hostname,
      port: 3306,
      username: DatabaseConfiguration.username,
      password: DatabaseConfiguration.password,
      database: DatabaseConfiguration.database,
      entities: [Player, Score]
    }),
    TypeOrmModule.forFeature([Player, Score])

  ],
  controllers: [AppController],
  providers: [AppService, PlayerService, ScoreService],
})
export class AppModule {}
